from flask import Flask
import os
import socket
import requests

app = Flask(__name__)

@app.route("/<int:id>")
def klungs(id):
    num = requests.get("http://server:3001/" + str(id))
    num = num.text
    return "hello\n" + num
 
    
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=3000)
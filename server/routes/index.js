var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/api', function(req, res){
  res.render('api', {
    title: 'Api'
  });
});

router.get('/:id(\\d+)/', function (req, res){
  // req.params.id is now defined here for you
  res.render('api', {
    title: req.params.id
  });
});


module.exports = router;
